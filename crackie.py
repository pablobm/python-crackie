import string

class Cracker:
  def __init__(self):
    self.basis = None
    self.test = None

  def crack(self):
    space = []
    for component in self.basis:
      space.append(generate_variants(component))
    finding = find_combination(space, self.test)
    if finding:
      return ''.join(finding)

def generate_variants(str):
  variants = set()
  add_first_letter(str, variants)
  add_mappings(str, variants)
  if len(str) > 1:
    add_letter_case_permutations(str, variants)
  return variants

def find_combination(space, test):
  counter_maxes = [len(posibilities)-1 for posibilities in space]
  counters = [0 for _ in space]
  components = [list(component) for component in space]
  while True:
    combination = [components[i][pos_counter] for i, pos_counter in enumerate(counters)]
    if test(combination):
      return combination

    carry = True
    for i, pos_counter in enumerate(counters):
      if carry:
        counters[i] += 1
      if counters[i] > counter_maxes[i]:
        counters[i] = 0
      else:
        carry = False
    if carry:
      break

MAPPINGS = {
  'one': ['1', '!'],
  'two': ['2', '@'],
  'three': ['3', '£'],
  'four': ['4', '$'],
  'five': ['5', '%'],
  'six': ['6', '^'],
  'seven': ['7', '&'],
  'eight': ['8', '*'],
  'nine': ['9', '('],
  'zero': ['0', ')'],
}

def add_first_letter(str, combinations):
  combinations.add(str[0].lower())
  combinations.add(str[0].upper())

def add_mappings(str, combinations):
  for match, mappings_for_match in MAPPINGS.items():
    if str.find(match) != -1:
      for mapping in mappings_for_match:
        new_variant = str.replace(match, mapping)
        combinations.add(new_variant)

def add_letter_case_permutations(str, combinations):
  chars = list(str)
  num_permutations = 2**len(chars)
  for i in range(num_permutations):
    new_chars = bitwise_uppercase(i, chars)
    combinations.add(''.join(new_chars))

def bitwise_uppercase(bitmask, chars):
  single_bit = 1
  chars_index = 0
  result = []
  for next_char in reversed(chars):
    if bitmask & single_bit:
      next_char = next_char.upper()
    result.insert(0, next_char)
    chars_index += 1
    single_bit <<= 1
  return result
