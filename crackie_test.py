import unittest
from unittest.mock import Mock
import crackie

class CrackieTestCase(unittest.TestCase):
  def test_generate_variants(self):
    actual = crackie.generate_variants('one')
    expected = {'o', 'O', '1', '!', 'one', 'onE', 'oNe', 'oNE', 'One', 'OnE', 'ONe', 'ONE'}
    self.assertSetEqual(expected, actual)

    actual = crackie.generate_variants('o')
    expected = {'o', 'O'}
    self.assertSetEqual(expected, actual)

    actual = crackie.generate_variants('!')
    expected = {'!'}
    self.assertSetEqual(expected, actual)

  def test_failed_find_combination(self):
    components = [{'a', 'A', 'aa'}, {'x'}, {'2', 'two'}]
    test = Mock(return_value=False)
    result = crackie.find_combination(components, test)
    self.assertEqual(None, result)
    test.assert_any_call(['a', 'x', '2'])
    test.assert_any_call(['a', 'x', 'two'])
    test.assert_any_call(['A', 'x', '2'])
    test.assert_any_call(['A', 'x', 'two'])
    test.assert_any_call(['aa', 'x', '2'])
    test.assert_any_call(['aa', 'x', 'two'])
    self.assertEqual(6, test.call_count)

  def test_successful_find_combination(self):
    class Trier:
      def __init__(self):
        self.num_tries = 0
      def three_tries(self, combination):
        if combination == ['aa', 'x', '2']:
          return True
    components = [{'a', 'A', 'aa'}, {'x'}, {'2', 'two'}]
    test = Mock(wraps=Trier().three_tries)
    result = crackie.find_combination(components, test)
    self.assertListEqual(['aa', 'x', '2'], result)

  def test_facade(self):
    def test_func(candidate):
      string = ''.join(candidate)
      return string == '1ChbsT'

    cracker = crackie.Cracker()
    cracker.basis = ["one", "c", "h", "b", "st"]
    cracker.test = test_func
    result = cracker.crack()
    self.assertEqual('1ChbsT', result)


if __name__ == '__main__':
  unittest.main()
